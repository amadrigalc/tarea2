package ucenfotec.ac.cr.bl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

public class CL {

    private ArrayList<Cliente> listaClientes;
    private ArrayList<Cuenta> listaCta;

    public CL(){
        listaClientes = new ArrayList<>();
        listaCta = new ArrayList<>();
    }

    public String crearCliente(String id, String name, LocalDate nacimiento, int edad, String direccion){
        Cliente nuevoCliente = new Cliente(id,name,nacimiento,edad,direccion);
        listaClientes.add(nuevoCliente);
        return "Cliente ha sido creado correctamente";
    }

    public ArrayList<String> listarClientes(){
        ArrayList lista = new ArrayList();

        for (Cliente clienteTemp:listaClientes) {
            lista.add(clienteTemp.toString());
        }
        return  lista;
    }

    public String crearCuenta(){
        Cuenta nuevoCuenta = new Cuenta();
        listaCta.add(nuevoCuenta);
        return "Cuenta ha sido creado correctamente";
    }

    public ArrayList<String> listarCta(){
        ArrayList lista = new ArrayList();

        for (Cliente clienteTemp:listaClientes) {
            lista.add(clienteTemp.toString());
        }
        return  lista;
    }


}
