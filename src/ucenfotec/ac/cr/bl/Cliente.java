package ucenfotec.ac.cr.bl;

import java.time.LocalDate;
import java.util.Date;

public class Cliente {
    private String id;
    private String name;
    private Date nacimiento;
    private int edad;
    private String direccion;

    public Cliente(String id, String name, LocalDate nacimiento, int edad, String direccion) {

    }

    public Cliente(String id, String name, Date nacimiento, int edad, String direccion) {
        this.id = id;
        this.name = name;
        this.nacimiento = nacimiento;
        this.edad = edad;
        this.direccion = direccion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getNacimiento() {
        return nacimiento;
    }

    public void setNacimiento(Date nacimiento) {
        this.nacimiento = nacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", nacimiento=" + nacimiento +
                ", edad=" + edad +
                ", direccion='" + direccion + '\'' +
                '}';
    }
}
