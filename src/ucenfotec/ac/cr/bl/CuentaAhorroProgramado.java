package ucenfotec.ac.cr.bl;

public class CuentaAhorroProgramado extends Cuenta{
    public CuentaAhorroProgramado() {
    }

    public CuentaAhorroProgramado(String id, Cliente cliente, String type, double saldo) {
        super(id, cliente, type, saldo);
    }

    @Override
    public String toString() {
        return "Esta cuenta es de tipo CuentaAhorroProgramado{} " + super.toString();
    }
}
