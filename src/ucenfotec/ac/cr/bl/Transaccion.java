package ucenfotec.ac.cr.bl;

import java.time.LocalDate;

public class Transaccion {

    private LocalDate fecha;
    private String descripcion;
    private double monto;
    private Cuenta cuentaAsociada;

    public Transaccion() {
    }

    public Transaccion(LocalDate fecha, String descripcion, double monto, Cuenta cuentaAsociada) {
        this.fecha = fecha;
        this.descripcion = descripcion;
        this.monto = monto;
        this.cuentaAsociada = cuentaAsociada;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public Cuenta getCuentaAsociada() {
        return cuentaAsociada;
    }

    public void setCuentaAsociada(Cuenta cuentaAsociada) {
        this.cuentaAsociada = cuentaAsociada;
    }

    @Override
    public String toString() {
        return "Transaccion{" +
                "fecha=" + fecha +
                ", descripcion='" + descripcion + '\'' +
                ", monto=" + monto +
                ", cuentaAsociada=" + cuentaAsociada +
                '}';
    }
}
