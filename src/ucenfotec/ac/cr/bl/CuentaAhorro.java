package ucenfotec.ac.cr.bl;

public class CuentaAhorro extends Cuenta{
    private double tasaInteres;

    public CuentaAhorro() {
    }

    public CuentaAhorro(String id, Cliente cliente, String type, double saldo, double tasaInteres) {
        super(id, cliente, type, saldo);
        this.tasaInteres = tasaInteres;
    }

    public double getTasaInteres() {
        return tasaInteres;
    }

    public void setTasaInteres(double tasaInteres) {
        this.tasaInteres = tasaInteres;
    }

    @Override
    public String toString() {
        return " Esta cuenta es de tipo CuentaAhorro{" +
                "tasaInteres=" + tasaInteres +
                "} " + super.toString();
    }
}
