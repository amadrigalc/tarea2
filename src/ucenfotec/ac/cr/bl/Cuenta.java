package ucenfotec.ac.cr.bl;

public class Cuenta {

    private String id;
    private Cliente cliente;
    private double saldo;


    public Cuenta() {
    }

    public Cuenta(String id, Cliente cliente, String type, double saldo) {
        this.id = id;
        this.cliente = cliente;
        this.saldo = saldo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    @Override
    public String toString() {
        return "Cuenta{" +
                "id='" + id + '\'' +
                ", cliente=" + cliente +
                ", saldo=" + saldo +
                '}';
    }
}
