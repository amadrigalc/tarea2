package ucenfotec.ac.cr.ui;

import ucenfotec.ac.cr.bl.CL;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;

public class UI {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static CL gestor = new CL();

    public static void main(String[] args) throws IOException {
        mostrarMenu();
    }
    public static void mostrarMenu() throws IOException {
        int opcion = -1;
        do{
            System.out.println("**** Bienvenido a LosLavadores.com *****");
            System.out.println("1. Registrar cliente");
            System.out.println("2. Listar clientes");
            System.out.println("3. Crear cuenta");
            System.out.println("4. Realizar depósito");
            System.out.println("5. Realizar retiro");
            System.out.println("6. Mostrar saldo en cuenta");
            System.out.println("0. Salir");
            System.out.print("Seleccione una opción: ");
            opcion = Integer.parseInt(in.readLine());
            procesarOpcion(opcion);
        }while(opcion !=0);
    }
    public static void procesarOpcion(int opcion) throws IOException {
        switch (opcion){
            case 1:
                registrarCliente();
                break;
            case 2:
                listarClientes();
                break;
            case 3:
                crearCuenta();
                break;
            case 4:
                realizarDeposito();
                break;
            case 5:
                realizarRetiro();
                break;
            case 6:
                mostrarSaldo();
                break;
            case 0:
                System.out.println("Gracias por su visita!!");
                break;
            default:
                System.out.println("Opción invalida!");
                break;
        }
    }

    private static void registrarCliente() throws IOException {

        System.out.println("Ingrese el nombre completo del cliente: ");
        String name = in.readLine();
        System.out.println("Ingrese el numero de identidad del cliente");
        String id = in.readLine();
        System.out.println("Ingrese la fecha de nacimiento del cliente:");
        LocalDate date = LocalDate.parse(in.readLine());
        System.out.println("Ingrese la direccion del cliente");
        String direccion = in.readLine();

        int edad = calcularEdad(date);

        gestor.crearCliente(id,name,date,edad,direccion);
    }


    private static int calcularEdad(LocalDate date){
        int edad = 0;

        return edad;

    }

    private static void listarClientes() {
        gestor.listarClientes();
    }

    private static void crearCuenta() {
    }

    private static void realizarDeposito() {
    }

    private static void realizarRetiro() {
    }

    private static void mostrarSaldo() {
    }


}
